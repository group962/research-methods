Type of Research question: Early.

Research Question: What are the image classifications techniques in machine learning used for facial recognition?

PICO elements:
=============
Context: The research question deals with the image classification in computer vision technology for face recognition. 
Computer vision is the field of AI that focuses on replicating parts of the complexity of the human vision system and
enabling computers to identify and process objects in images and videos in the same way that humans do.
The image classification for face recognition is one of the applications of computer vision.

Problem or Population: Face recognition software’s have become widely popular and places a crucial role in biometrics and surveillance.
This research aims to find out different techniques which are used in facial recognition. 
This helps to find out the merits and demerits of the algorithms that are used for face recognition. 
The population include the images of face of people. The face recognition software requires image of face of people in different lighting conditions, different situations etc. 
This collection of face becomes our population.

Intervention, independent (= manipulated) variable, Exposure, Prognostic Factor:
The image classification techniques that are used are the experimental factors. This is because the image classification techniques used for face recognition are compared with each other.

Comparison: The comparison between image classification techniques is carried out. The performance of the different classification algorithms is compared with each other.

Outcomes: The performance parameter accuracy is used as the outcome. The accuracy helps us to compare the performance difference between the image classification algorithms. 